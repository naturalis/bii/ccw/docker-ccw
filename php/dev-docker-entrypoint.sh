#!/bin/sh
set -e

if ! getent hosts host.docker.internal > /dev/null 2>&1; then
  docker_host=$(ip -4 route list match 0/0 | awk '{print $3}')
  echo "$docker_host host.docker.internal" >> /etc/hosts
fi

exec "$@"
