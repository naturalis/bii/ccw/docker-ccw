# Build PHP - basic image
FROM php:8.3-fpm-alpine as fpm

WORKDIR /var/www

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions \
	/usr/local/bin/

# hadolint ignore=DL3017,DL3018,SC2086
RUN set -eux; \
    apk upgrade --no-cache; \
    apk add --no-cache \
        mysql-client; \
    mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"; \
    sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 10M/' /usr/local/etc/php/php.ini; \
    sed -i 's/post_max_size = 8M/post_max_size = 12M/' /usr/local/etc/php/php.ini; \
    chmod +x /usr/local/bin/install-php-extensions; \
    install-php-extensions mysqli opcache apcu gd zip bz2; \
    rmdir /var/www/html;

# Build PHP - include composer
FROM fpm as build

RUN install-php-extensions @composer

# Build PHP - include xdebug
FROM build as dev

# hadolint ignore=DL3018,SC2086
RUN set -eux; \
    install-php-extensions xdebug; \
    printf "%s\n" "xdebug.mode=debug" "xdebug.client_host=host.docker.internal" "xdebug.client_port=9000" \
    >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini;

COPY dev-docker-entrypoint.sh /usr/local/bin/docker-php-entrypoint
